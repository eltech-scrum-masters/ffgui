#!/usr/bin/env bash
for filename in ./FFGUI/UI/*.ui; do
  pyuic5 "$filename" -o $(echo "$filename" | sed 's/\.ui/\.py/')
done