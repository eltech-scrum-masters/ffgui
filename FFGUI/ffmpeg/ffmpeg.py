import subprocess
import re, os
import FFGUI.Logging as Logging
from PyQt5.QtCore import QObject, pyqtSignal
from enum import Enum
from typing import Tuple, List, Callable

FFMPEG_COMMAND = 'ffmpeg'
FFPROBE_COMMAND = 'ffprobe'

valid_input_extensions = ' '.join(['*.3dostr', '*.3g2', '*.3gp', '*.4xm', '*.a64', '*.aa', '*.aac', '*.ac3', '*.acm',
                                   '*.act', '*.adf', '*.adp', '*.ads', '*.adts', '*.adx', '*.aea', '*.afc', '*.aiff',
                                   '*.aix', '*.alaw', '*.alias_pix', '*.amr', '*.amrnb', '*.amrwb', '*.anm', '*.apc',
                                   '*.ape', '*.apng', '*.aptx', '*.aptx_hd', '*.aqtitle', '*.asf', '*.asf_o',
                                   '*.asf_stream', '*.ass', '*.ast', '*.au', '*.avi', '*.avisynth', '*.avm2', '*.avr',
                                   '*.avs', '*.avs2', '*.bethsoftvid', '*.bfi', '*.bfstm', '*.bin', '*.bink', '*.bit',
                                   '*.bmp_pipe', '*.bmv', '*.boa', '*.brender_pix', '*.brstm', '*.c93', '*.caf',
                                   '*.cavsvideo', '*.cdg', '*.cdxl', '*.cine', '*.codec2', '*.codec2raw', '*.concat',
                                   '*.crc', '*.dash', '*.data', '*.daud', '*.dcstr', '*.dds_pipe', '*.dfa', '*.dirac',
                                   '*.dnxhd', '*.dpx_pipe', '*.dsf', '*.dshow', '*.dsicin', '*.dss', '*.dts', '*.dtshd',
                                   '*.dv', '*.dvbsub', '*.dvbtxt', '*.dvd', '*.dxa', '*.ea', '*.ea_cdata', '*.eac3',
                                   '*.epaf', '*.exr_pipe', '*.f32be', '*.f32le', '*.f4v', '*.f64be', '*.f64le',
                                   '*.ffmetadata', '*.fifo', '*.fifo_test', '*.film_cpk', '*.filmstrip', '*.fits',
                                   '*.flac', '*.flic', '*.flv', '*.framecrc', '*.framehash', '*.framemd5', '*.frm',
                                   '*.fsb', '*.g722', '*.g723_1', '*.g726', '*.g726le', '*.g729', '*.gdigrab', '*.gdv',
                                   '*.genh', '*.gif', '*.gsm', '*.gxf', '*.h261', '*.h263', '*.h264', '*.hash', '*.hds',
                                   '*.hevc', '*.hls', '*.hls','*.applehttp', '*.hnm', '*.ico', '*.idcin', '*.idf',
                                   '*.iff', '*.ilbc', '*.image2', '*.image2pipe', '*.ingenient', '*.ipmovie', '*.ipod',
                                   '*.ircam', '*.ismv', '*.iss', '*.iv8', '*.ivf', '*.ivr', '*.j2k_pipe', '*.jacosub',
                                   '*.jpeg_pipe', '*.jpegls_pipe', '*.jv', '*.latm', '*.lavfi', '*.live_flv', '*.lmlm4',
                                   '*.loas', '*.lrc', '*.lvf', '*.lxf', '*.m4v', '*.mkv', '*.matroska', '*.webm',
                                   '*.md5', '*.mgsts', '*.microdvd', '*.mjpeg', '*.mjpeg_2000', '*.mkvtimestamp_v2',
                                   '*.mlp', '*.mlv', '*.mm', '*.mmf', '*.mov', '*.mov', '*.mp4', '*.m4a', '*.3gp',
                                   '*.mp2', '*.mp3', '*.mp4', '*.mpc', '*.mpc8', '*.mpeg', '*.mpeg1video',
                                   '*.mpeg2video', '*.mpegts', '*.mpegtsraw', '*.mpegvideo', '*.mpjpeg', '*.mpl2',
                                   '*.mpsub', '*.msf', '*.msnwctcp', '*.mtaf', '*.mtv', '*.mulaw', '*.musx', '*.mv',
                                   '*.mvi', '*.mxf', '*.mxf_d10', '*.mxf_opatom', '*.mxg', '*.nc', '*.nistsphere',
                                   '*.nsp', '*.nsv', '*.null', '*.nut', '*.nuv', '*.oga', '*.ogg', '*.ogv', '*.oma',
                                   '*.opus', '*.paf', '*.pam_pipe', '*.pbm_pipe', '*.pcx_pipe', '*.pgm_pipe',
                                   '*.pgmyuv_pipe', '*.pictor_pipe', '*.pjs', '*.pmp', '*.png_pipe', '*.ppm_pipe',
                                   '*.psd_pipe', '*.psp', '*.psxstr', '*.pva', '*.pvf', '*.qcp', '*.qdraw_pipe',
                                   '*.r3d', '*.rawvideo', '*.realtext', '*.redspark', '*.rl2', '*.rm', '*.roq', '*.rpl',
                                   '*.rsd', '*.rso', '*.rtp', '*.rtp_mpegts', '*.rtsp', '*.s16be', '*.s16le', '*.s24be',
                                   '*.s24le', '*.s32be', '*.s32le', '*.s337m', '*.s8', '*.sami', '*.sap', '*.sbc',
                                   '*.sbg', '*.scc', '*.sdl', '*.sdl2', '*.sdp', '*.sdr2', '*.sds', '*.sdx',
                                   '*.segment', '*.ser', '*.sgi_pipe', '*.shn', '*.siff', '*.singlejpeg', '*.sln',
                                   '*.smjpeg', '*.smk', '*.smoothstreaming', '*.smush', '*.sol', '*.sox', '*.spdif',
                                   '*.spx', '*.srt', '*.stl', '*.stream_segment', '*.s', '*.subviewer', '*.subviewer1',
                                   '*.sunrast_pipe', '*.sup', '*.svag', '*.svcd', '*.svg_pipe', '*.swf', '*.tak',
                                   '*.tedcaptions', '*.tee', '*.thp', '*.tiertexseq', '*.tiff_pipe', '*.tmv',
                                   '*.truehd', '*.tta', '*.tty', '*.txd', '*.ty', '*.u16be', '*.u16le', '*.u24be',
                                   '*.u24le', '*.u32be', '*.u32le', '*.u8', '*.uncodedframecrc', '*.v210', '*.v210x',
                                   '*.vag', '*.vc1', '*.vc1test', '*.vcd', '*.vfwcap', '*.vidc', '*.vivo', '*.vmd',
                                   '*.vob', '*.vobsub', '*.voc', '*.vpk', '*.vplayer', '*.vqf', '*.w64', '*.wav',
                                   '*.wc3movie', '*.webm', '*.webm_chunk', '*.webm_dash_manife', '*.webp',
                                   '*.webp_pipe', '*.webvtt', '*.wsaud', '*.wsd', '*.wsvqa', '*.wtv', '*.wv', '*.wve',
                                   '*.xa', '*.xbin', '*.xmv', '*.xpm_pipe', '*.xvag', '*.xwd_pipe', '*.xwma', '*.yop',
                                   '*.yuv4mpegpipe'])


def get_libs() -> [str]:
    proc = subprocess.run([FFMPEG_COMMAND, '-version'], text=True, capture_output=True)
    return [re.match(r'\w+', line).group(0) for line in proc.stdout.splitlines()[3:]]


class Filter:
    supports_timeline: bool
    supports_slice_threading: bool
    supports_additional_commands: bool

    class Node(Enum):
        AUDIO = 1
        VIDEO = 2
        DYNAMIC = 3
        SINK = 4

    input_type: Tuple[Node, ...]
    output_type: Tuple[Node, ...]
    name: str

    def __init__(self,
                 name: str,
                 description: str,
                 input_type: Tuple[Node, ...],
                 output_type: Tuple[Node, ...],
                 supports_timeline: bool,
                 supports_slice_threading: bool,
                 supports_additional_commands: bool):
        self.input_type = input_type
        self.output_type = output_type
        self.name = name
        self.description = description
        self.supports_timeline = supports_timeline
        self.supports_slice_threading = supports_slice_threading
        self.supports_additional_commands = supports_additional_commands

    description: str

    def parse_flags(self, args: str) -> 'Filter':
        self.supports_timeline = False
        self.supports_slice_threading = False
        self.supports_additional_commands = False

        if args[0] == 'T':
            self.supports_timeline = True
        if args[1] == 'S':
            self.supports_slice_threading = True
        if args[2] == 'C':
            self.supports_additional_commands = True
        return self

    def parse_graph(self, args: str) -> 'Filter':

        def token_to_node_type(token: chr) -> 'Filter.Node':
            if token == 'A':
                return Filter.Node.AUDIO
            if token == 'V':
                return Filter.Node.VIDEO
            if token == 'N':
                return Filter.Node.DYNAMIC
            if token == 'I':
                return Filter.Node.SINK

        sides = args.split('->')
        self.input_type = tuple(map(token_to_node_type, sides[0]))
        self.output_type = tuple(map(token_to_node_type, sides[1]))
        return self

    @staticmethod
    def from_match(match: re.Match) -> 'Filter':
        return Filter(match.group(2),
                      match.group(4),
                      None,
                      None,
                      None,
                      None,
                      None).parse_flags(match.group(1)).parse_graph(match.group(3))


def get_filters() -> [Filter]:
    logger = Logging.get_logger
    try:
        proc = subprocess.run([FFMPEG_COMMAND, '-filters'], text=True, capture_output=True)
        logger.info('Loading ffmpeg filter list')
        res = [Filter.from_match(match) for match in
               [re.match(r'\s([.TSC]{3})\s(\w+)\s*([AVNI]+->[AVNI])+\s*((.+\b\s)*\w+\.)', line)
                for line
                in proc.stdout.splitlines()]
               if match is not None]
        return res
    except Exception as e:
        logger.error("Unexpected ffmpeg behavior: " + str(e))


def run(args: List[str]) -> subprocess.CompletedProcess:
    return subprocess.run([FFMPEG_COMMAND] + args, text=True, capture_output=True)


def run_realtime(args: List[str], add_output_line: Callable[[str], None]) -> None:
    com = [FFMPEG_COMMAND] + args
    proc = subprocess.Popen(com, shell=False, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
    get_output(proc, add_output_line)


class FfmpegCommandBuilder(QObject):
    command_updated = pyqtSignal(name='commandUpdated')

    def __init__(self):
        super().__init__()
        self.input_files = []

    def add_input_file(self, path: str) -> None:
        self.input_files.append(path)
        self.command_updated.emit()

    def remove_input_file(self, index: int) -> None:
        self.input_files.pop(index)
        self.command_updated.emit()

    def clear(self) -> None:
        self.input_files = []

    def build_command(self) -> [str]:
        command = []
        for path in self.input_files:
            command.append('-i')
            command.append(path.replace(" ", r"\ "))
        return command


def get_file_info(path_to_file: str):

    proc = subprocess.run([FFPROBE_COMMAND, '-hide_banner', path_to_file], text=True, capture_output=True)
    res = proc.stderr
    return res


def get_file_info_realtime(path_to_file: str, show_output: Callable[[str], None]) -> None:
    proc = subprocess.Popen([FFPROBE_COMMAND, '-hide_banner', path_to_file], shell=False,
                                                                             stderr=subprocess.STDOUT,
                                                                             stdout=subprocess.PIPE)
    get_output(proc, show_output)


def get_output(proc: subprocess.Popen, show_output: Callable[[str], None]) -> None:
    buff = ''
    while True:
        out = proc.stdout.read(1).decode("UTF-8")
        if out == '' and proc.poll() is not None:
            break
        if out != '':
            if out == '\n':
                show_output(buff)
                buff = ''
            else:
                buff += out
