from unittest import TestCase
from FFGUI.ffmpeg.ffmpeg import Filter
import re

class TestFilter(TestCase):
    def test_parse_flags(self):
        test_filter = Filter(None, None, None, None, None, None, None)
        # testFilter
        self.assertEqual(Filter(None, None, None, None, False, False, False).__dict__,
                         test_filter.parse_flags('...').__dict__)

        self.assertEqual(Filter(None, None, None, None, True, False, False).__dict__,
                         test_filter.parse_flags('T..').__dict__)

        self.assertEqual(Filter(None, None, None, None, False, True, False).__dict__,
                         test_filter.parse_flags('.S.').__dict__)

        self.assertEqual(Filter(None, None, None, None, False, False, True).__dict__,
                         test_filter.parse_flags('..C').__dict__)

        self.assertEqual(Filter(None, None, None, None, True, True, True).__dict__,
                         test_filter.parse_flags('TSC').__dict__)

    def test_parse_graph(self):
        Node = Filter.Node
        test_filter = Filter(None, None, None, None, None, None, None)
        self.assertEqual(Filter(None, None, (Node.AUDIO,), (Node.AUDIO,), None, None, None).__dict__,
                         test_filter.parse_graph('A->A').__dict__)
        self.assertEqual(Filter(None, None, (Node.VIDEO,), (Node.AUDIO,), None, None, None).__dict__,
                         test_filter.parse_graph('V->A').__dict__)
        self.assertEqual(Filter(None, None, (Node.DYNAMIC,), (Node.AUDIO,), None, None, None).__dict__,
                         test_filter.parse_graph('N->A').__dict__)
        self.assertEqual(Filter(None, None, (Node.SINK,), (Node.AUDIO,), None, None, None).__dict__,
                         test_filter.parse_graph('I->A').__dict__)
        self.assertEqual(Filter(None, None, (Node.SINK, Node.AUDIO), (Node.AUDIO,), None, None, None).__dict__,
                         test_filter.parse_graph('IA->A').__dict__)

    def test_from_match(self):
        Node = Filter.Node
        line = ' ... abench            A->A       Benchmark part of a filtergraph.'
        match = re.match(r'\s([.TSC]{3})\s(\w+)\s*([AVNI]+->[AVNI])+\s*((.+\b\s)*\w+\.)', line)
        test_filter = Filter.from_match(match)
        self.assertEqual(Filter(name='abench',
                                description='Benchmark part of a filtergraph.',
                                input_type=(Node.AUDIO,),
                                output_type=(Node.AUDIO,),
                                supports_timeline=False,
                                supports_slice_threading=False,
                                supports_additional_commands=False).__dict__, test_filter.__dict__)
