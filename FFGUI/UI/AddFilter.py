# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './FFGUI/UI/AddFilter.ui'
#
# Created by: PyQt5 UI code generator 5.12
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_AddFilterDialog(object):
    def setupUi(self, AddFilterDialog):
        AddFilterDialog.setObjectName("AddFilterDialog")
        AddFilterDialog.resize(832, 332)
        self.verticalLayout = QtWidgets.QVBoxLayout(AddFilterDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.tableWidget = QtWidgets.QTableWidget(AddFilterDialog)
        self.tableWidget.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.tableWidget.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.tableWidget.setShowGrid(False)
        self.tableWidget.setGridStyle(QtCore.Qt.NoPen)
        self.tableWidget.setCornerButtonEnabled(False)
        self.tableWidget.setColumnCount(7)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(4, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(5, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(6, item)
        self.tableWidget.horizontalHeader().setCascadingSectionResizes(False)
        self.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.tableWidget.verticalHeader().setVisible(False)
        self.tableWidget.verticalHeader().setStretchLastSection(False)
        self.verticalLayout.addWidget(self.tableWidget)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.AddFilterAdd = QtWidgets.QPushButton(AddFilterDialog)
        self.AddFilterAdd.setObjectName("AddFilterAdd")
        self.horizontalLayout.addWidget(self.AddFilterAdd)
        self.AddFilterCancel = QtWidgets.QPushButton(AddFilterDialog)
        self.AddFilterCancel.setObjectName("AddFilterCancel")
        self.horizontalLayout.addWidget(self.AddFilterCancel)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(AddFilterDialog)
        QtCore.QMetaObject.connectSlotsByName(AddFilterDialog)

    def retranslateUi(self, AddFilterDialog):
        _translate = QtCore.QCoreApplication.translate
        AddFilterDialog.setWindowTitle(_translate("AddFilterDialog", "Dialog"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("AddFilterDialog", "Name"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("AddFilterDialog", "Description"))
        item = self.tableWidget.horizontalHeaderItem(2)
        item.setText(_translate("AddFilterDialog", "Inputs"))
        item = self.tableWidget.horizontalHeaderItem(3)
        item.setText(_translate("AddFilterDialog", "Outputs"))
        item = self.tableWidget.horizontalHeaderItem(4)
        item.setText(_translate("AddFilterDialog", "Supports timeline"))
        item = self.tableWidget.horizontalHeaderItem(5)
        item.setText(_translate("AddFilterDialog", "Supports slice threading"))
        item = self.tableWidget.horizontalHeaderItem(6)
        item.setText(_translate("AddFilterDialog", "Supports additional commands"))
        self.AddFilterAdd.setText(_translate("AddFilterDialog", "Add"))
        self.AddFilterCancel.setText(_translate("AddFilterDialog", "Cancel"))


