# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './FFGUI/UI/FeedbackDialog.ui'
#
# Created by: PyQt5 UI code generator 5.12
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_FeedbackDialog(object):
    def setupUi(self, FeedbackDialog):
        FeedbackDialog.setObjectName("FeedbackDialog")
        FeedbackDialog.resize(321, 125)
        self.verticalLayout = QtWidgets.QVBoxLayout(FeedbackDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.Text = QtWidgets.QLabel(FeedbackDialog)
        self.Text.setAlignment(QtCore.Qt.AlignCenter)
        self.Text.setOpenExternalLinks(True)
        self.Text.setObjectName("Text")
        self.verticalLayout.addWidget(self.Text)
        self.ButtonBox = QtWidgets.QDialogButtonBox(FeedbackDialog)
        self.ButtonBox.setOrientation(QtCore.Qt.Horizontal)
        self.ButtonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Close)
        self.ButtonBox.setCenterButtons(True)
        self.ButtonBox.setObjectName("ButtonBox")
        self.verticalLayout.addWidget(self.ButtonBox)

        self.retranslateUi(FeedbackDialog)
        self.ButtonBox.accepted.connect(FeedbackDialog.accept)
        self.ButtonBox.rejected.connect(FeedbackDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(FeedbackDialog)

    def retranslateUi(self, FeedbackDialog):
        _translate = QtCore.QCoreApplication.translate
        FeedbackDialog.setWindowTitle(_translate("FeedbackDialog", "Dialog"))
        self.Text.setText(_translate("FeedbackDialog", "<html><head/><body><p>Feedback can be sent to <a href=\"mailto:incoming+eltech-scrum-masters-ffgui-11169567-issue-@incoming.gitlab.com\">this email</a>.</p></body></html>"))


