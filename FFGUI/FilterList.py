from FFGUI.UI.AddFilter import Ui_AddFilterDialog
from FFGUI.ffmpeg.ffmpeg import Filter
from FFGUI.ffmpeg import ffmpeg
from FFGUI.Logging import get_logger

from PyQt5.Qt import QDialog, QHeaderView, QAbstractItemView, QTableWidgetItem

from typing import Tuple


logger = get_logger


class FilterList(QDialog):
    def __init__(self, parent):
        logger.info('Constructing filter list dialog')
        super().__init__(parent=parent)

        self.ui = Ui_AddFilterDialog()
        ui = self.ui
        ui.setupUi(self)

        ui.AddFilterCancel.clicked.connect(self.close)

        filter_table = ui.tableWidget

        filter_table.setEditTriggers(QAbstractItemView.NoEditTriggers)
        filter_table.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
        filter_table.verticalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)

        def format_nodes(nodes: Tuple[Filter.Node, ...]) -> str:

            def name_node(node: Filter.Node) -> str:
                Node = Filter.Node
                if node == Node.AUDIO:
                    return 'Audio'
                elif node == Node.VIDEO:
                    return 'Video'
                elif node == Node.DYNAMIC:
                    return 'Dynamic'
                elif node == Node.SINK:
                    return 'Sink'
                else:
                    return 'Null'

            return '\n'.join([name_node(node) for node in nodes])

        def yes_no(flag: bool) -> str:
            if flag:
                return 'yes'
            else:
                return 'no'

        filters = ffmpeg.get_filters()

        for filter_obj in filters:
            filter_table.insertRow(filter_table.rowCount())
            filter_table.setItem(filter_table.rowCount()-1,
                                 0,
                                 QTableWidgetItem(filter_obj.name))
            filter_table.setItem(filter_table.rowCount() - 1,
                                 1,
                                 QTableWidgetItem(filter_obj.description))
            filter_table.setItem(filter_table.rowCount() - 1,
                                 2,
                                 QTableWidgetItem(format_nodes(filter_obj.input_type)))
            filter_table.setItem(filter_table.rowCount() - 1,
                                 3,
                                 QTableWidgetItem(format_nodes(filter_obj.output_type)))
            filter_table.setItem(filter_table.rowCount() - 1,
                                 4,
                                 QTableWidgetItem(yes_no(filter_obj.supports_timeline)))
            filter_table.setItem(filter_table.rowCount() - 1,
                                 5,
                                 QTableWidgetItem(yes_no(filter_obj.supports_slice_threading)))
            filter_table.setItem(filter_table.rowCount() - 1,
                                 6,
                                 QTableWidgetItem(yes_no(filter_obj.supports_additional_commands)))
        self.update()