import datetime
import logging
import logging.handlers as handlers

class Logger:
    def getLogger(self):
       logger=logging.getLogger('')
       if not len(logger.handlers):
          logger.setLevel(logging.INFO)
          now = datetime.datetime.now()
          formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
          logHandler = handlers.RotatingFileHandler('Logs ' + now.strftime("%Y-%m-%d") + '.log',
                                                    maxBytes=2000000,
                                                    backupCount=0)
          logHandler.setLevel(logging.INFO)
          logHandler.setFormatter(formatter)
          errorLogHandler = handlers.RotatingFileHandler('Errors ' + now.strftime("%Y-%m-%d") + '.log',
                                                         maxBytes=1000000,
                                                         backupCount=0)
          errorLogHandler.setLevel(logging.ERROR)
          errorLogHandler.setFormatter(formatter)


          logger.addHandler(logHandler)
          logger.addHandler(errorLogHandler)
       return logger

# general app logger
get_logger = Logger.getLogger(Logger)
