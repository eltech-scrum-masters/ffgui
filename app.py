from FFGUI import Logging
from FFGUI.FilterList import FilterList
from FFGUI.ffmpeg import ffmpeg
from FFGUI.ffmpeg.ffmpeg import FfmpegCommandBuilder
from FFGUI.UI.MainWindow import Ui_MainWindow
from FFGUI.UI.FeedbackDialog import Ui_FeedbackDialog

from PyQt5.Qt import QApplication, QMainWindow, QTableWidgetItem, QAbstractItemView, QMessageBox, QDialog
from PyQt5.QtCore import Qt, QUrl, pyqtSignal
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWebEngineWidgets import QWebEngineView

import os
import sys
import re

logger = Logging.get_logger


class MainWindow(QMainWindow):
    def __init__(self):
        logger.info('Constructing main FFGUI window')

        super().__init__()

        logger.info('Setting up main FFGUI window UI')
        self.ui = Ui_MainWindow()
        ui = self.ui
        ui.setupUi(self)
        ui.TabWidget.setCurrentIndex(0)
        ui.InputFilesTable.setSelectionBehavior(QAbstractItemView.SelectRows)

        # Tooltips
        ui.TabWidget.setTabToolTip(0, 'Check added files')
        ui.TabWidget.setTabToolTip(1, 'Manage available filters')
        ui.TabWidget.setTabToolTip(2, 'Information about selected file')
        ui.DeleteFileButton.setToolTip('Remove file from the list')
        ui.AddFileButton.setToolTip('Add needed file to the files list')
        ui.ManualButton.setToolTip('Open offline manual')
        ui.AboutButton.setToolTip('Basic program information')
        ui.FeedbackButton.setToolTip('Send feedback')
        ui.ffmpeg_command_line.setToolTip('Ffmpeg command line')
        ui.ffmpeg_edit_checkbox.setToolTip('Check to be able to use Ffmpeg command line')
        ui.ffmpeg_run_button.setToolTip('Run command line instructions')
        ui.AddFilterButton.setToolTip('Add filter to the active filters list')
        ui.SettingsButton.setToolTip('Open program settings')

        # TODO: Rewrite to spawning separate window, currently in debt
        self.win = Browser()

        ui.AddFilterButton.clicked.connect(self.spawn_fiter_list_event)
        ui.ManualButton.clicked.connect(self.spawn_documentation_browser)
        ui.ffmpeg_run_button.clicked.connect(self.run_command)
        ui.AddFileButton.clicked.connect(self.browse_file)
        ui.InputFilesTable.doubleClicked.connect(self.show_file_info)
        ui.ffmpeg_edit_checkbox.stateChanged.connect(self.enable_command_editing)
        ui.DeleteFileButton.clicked.connect(self.delete_input_file)
        ui.FeedbackButton.clicked.connect(self.spawn_feedback_dialog)

        self.command_builder = FfmpegCommandBuilder()
        self.command_builder.command_updated.connect(self.update_command)
        self.file_added.connect(self.command_builder.add_input_file)

    file_added = pyqtSignal(str, name='fileAdded')

    def update_command(self):
        ui = self.ui
        ui.ffmpeg_command_line.setText(' '.join(self.command_builder.build_command()))

    def delete_input_file(self):
        ui = self.ui
        if ui.InputFilesTable.rowCount() > 0:
            ui.InputFilesTable.setCurrentCell(ui.InputFilesTable.currentItem().row(), 0)
            file_name = ui.InputFilesTable.currentItem().text()
            ui.InputFilesTable.setCurrentCell(ui.InputFilesTable.currentItem().row(), 1)
            path_to_file = ui.InputFilesTable.currentItem().text()
            question = QMessageBox.question(self, 'Delete File',
                                            'Are you sure you want to remove this file from list?\n'
                                            'File: '+file_name + '\n'
                                            'File path: '+path_to_file + '\n',
                                            QMessageBox.Yes | QMessageBox.No,
                                            QMessageBox.No)
            if question == QMessageBox.Yes:
                self.command_builder.remove_input_file(ui.InputFilesTable.currentItem().row())
                ui.InputFilesTable.removeRow(ui.InputFilesTable.currentItem().row())

    def spawn_fiter_list_event(self):
        logger.info('Spawning filter list dialog')
        filter_list_w = FilterList(self)
        filter_list_w.show()

    def browse_file(self):
        ui = self.ui
        file_path = QFileDialog.getOpenFileName(self, 'Navigate File', "/home", ffmpeg.valid_input_extensions)
        if file_path[0]:
            check_repetition = False
            repetition_item = 0
            for i in range(0, ui.InputFilesTable.rowCount()):
                ui.InputFilesTable.setCurrentCell(i, 0)
                print(ui.InputFilesTable.currentItem().text())
                if ui.InputFilesTable.currentItem().text() == os.path.basename(file_path[0]):
                    check_repetition = True
                    repetition_item = i
            if check_repetition is False:
                ui.InputFilesTable.insertRow(ui.InputFilesTable.rowCount())
                ui.InputFilesTable.setItem(ui.InputFilesTable.rowCount()-1, 0, QTableWidgetItem(os.path.basename(file_path[0])))
                ui.InputFilesTable.setItem(ui.InputFilesTable.rowCount()-1, 1, QTableWidgetItem(file_path[0]))
                self.file_added.emit(file_path[0])
                if ui.InputFilesTable.rowCount() > 1:
                    ui.InputFilesTable.selectRow(ui.InputFilesTable.rowCount()-1)
            else:
                ui.InputFilesTable.selectRow(repetition_item)

    def enable_command_editing(self, state):
        if state == Qt.Checked:
            question = QMessageBox.question(self, 'Advanced option.',
                                            'This option is only recommended for an advanced user who knows how to '
                                            'manually use ffmpeg.\n'
                                            'Are you sure?',
                                            QMessageBox.Yes | QMessageBox.No,
                                            QMessageBox.No)
            if question == QMessageBox.Yes:
                self.ui.ffmpeg_command_line.setReadOnly(False)
                self.ui.ffmpeg_edit_checkbox.setDisabled(True)
            else:
                self.ui.ffmpeg_edit_checkbox.setCheckState(Qt.Unchecked)

    def spawn_feedback_dialog(self) -> None:
        self.feedback = FeedBackDialog()

    def run_command(self) -> None:
        self.ui.TabWidget.setCurrentIndex(2)
        self.ui.textEdit_FileInfo.clear()

        def add_text(s):
            self.ui.textEdit_FileInfo.append(s)
        args = self.get_command_args()
        ffmpeg.run_realtime(args, add_text)

    def get_command_args(self) -> [str]:
        if self.ui.ffmpeg_edit_checkbox.checkState() == Qt.Checked:
            args = [arg.replace("\\", "")
                    for arg
                    in re.split(r"(?<!\\) ",
                                self.ui.ffmpeg_command_line.text())
                    if arg is not None]
        else:
            args = self.command_builder.build_command()
        return args

    def show_file_info(self):
        ui = self.ui
        ui.TabWidget.setCurrentIndex(2)
        ui.InputFilesTable.setCurrentCell(ui.InputFilesTable.currentItem().row(), 1)
        path_to_file = ui.InputFilesTable.currentItem().text()
        ui.textEdit_FileInfo.clear()

        def add_text(s):
            ui.textEdit_FileInfo.append(s)
        ffmpeg.get_file_info_realtime(path_to_file, add_text)

    def spawn_documentation_browser(self):
        import os
        url = QUrl.fromLocalFile(os.path.abspath(r"./manual/ffmpeg-all.html"))
        logger.info("Trying to load manual @" + str(url))
        self.win.load(QUrl(url))
        self.win.show()


class Browser(QWebEngineView):
    def __init__(self):
        super().__init__()


class FeedBackDialog(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_FeedbackDialog()
        self.ui.setupUi(self)
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    win = MainWindow()
    win.show()
    sys.exit(app.exec())
